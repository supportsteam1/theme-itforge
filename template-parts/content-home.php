<?php 
get_header();
?>   
<section id="congratulations">
	<div class="hello_visit" style="background: url(<?php echo get_template_directory_uri(); ?>/img/back.jpg);">
		<div class="alert alert-warning" role="alert">
  			Вітаємо!
		</div>
	</div>
</section>
<section id="for_style">
	<section id="easter_notes">
		<div class="notes">
			<h3><b>Великдень</b></h3>
			<p>Для українців Великдень – це не лише різнокольорові писанки і смачна паска. Це особливе свято, коли слідують традиціям і вірять у диво. Існує безліч повір’їв і застережень на Великдень. Дотримуючись їх, усе бажане здійснюється.</p>
		</div>
		<div class="eas_p">
			<img src="<?php echo get_template_directory_uri(); ?>/img/Rectangle 32.png">
		</div>
	</section>
</section>
<section id="best_works">
	<div class="works_b">
		<h1>Наші кращі роботи</h1>
	</div>
	<div class="layout">
		<div id="carouselEaster" class="carousel slide" data-ride="carousel">
		  <div class="carousel-inner" id="carouselEasterBox">
		    <div class="carousel-item active">
		      <img src="<?php echo get_template_directory_uri(); ?>/img/пасха1.jpeg" class="d-block w-100" alt="east-basket1">
		    </div>
		    <div class="carousel-item">
		      <img src="<?php echo get_template_directory_uri(); ?>/img/пасха2.jpg" class="d-block w-100" alt="east-basket2">
		    </div>
		    <div class="carousel-item">
		      <img src="<?php echo get_template_directory_uri(); ?>/img/пасха3.jpeg" class="d-block w-100" alt="east-basket3">
		    </div>
		    <div class="carousel-item">
		      <img src="<?php echo get_template_directory_uri(); ?>/img/пасха4.jpeg" class="d-block w-100" alt="east-basket4">
		    </div>
		    <div class="carousel-item">
		      <img src="<?php echo get_template_directory_uri(); ?>/img/пасха5.jpeg" class="d-block w-100" alt="east-basket5">
		    </div>
		  </div>
		  <a class="carousel-control-prev" href="#carouselEaster" role="button" data-slide="prev">
		    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
		    <span class="sr-only">Previous</span>
		  </a>
		  <a class="carousel-control-next" href="#carouselEaster" role="button" data-slide="next">
		    <span class="carousel-control-next-icon" aria-hidden="true"></span>
		    <span class="sr-only">Next</span>
		  </a>
		</div>
	</div>
</section>
<?php 
	get_footer();


?> 