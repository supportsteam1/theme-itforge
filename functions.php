<?php 
add_action('wp_enqueue_scripts', 'style_theme');
add_action('wp_enqueue_scripts', 'script_theme');
add_action('after_setup_theme', 'reg_menu');

function  reg_menu() 
{
	register_nav_menu('top', 'Меню в шапке');
	register_nav_menu('botton', 'Меню в подвале');
}

function style_theme() { wp_enqueue_style('style', get_stylesheet_uri());

 wp_enqueue_style( 'booTstrap', 'https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css' ); 
 wp_enqueue_style( 'main_style', get_template_directory_uri() . '/css/main.css' );
 wp_enqueue_style( 'fonts', 'https://fonts.googleapis.com/css?family=Philosopher:400,700&display=swap&subset=cyrillic' );
}


function script_theme()
{
	wp_deregister_script('jquery'); 
	
	wp_register_script('jquery', 'http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js');

	wp_enqueue_script('jquery');
	
	wp_enqueue_script('flexslider', get_template_directory_uri() . '/js/jquery.flexslider.js', ['jquery'], null, true);

	wp_enqueue_script('booTstrap', 'https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js',  true);

	wp_enqueue_script('DRAG', get_template_directory_uri() . '/js/drag.js',  true);


}


/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}

?>

