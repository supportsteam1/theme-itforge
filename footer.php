<?php wp_footer(); ?>
<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package easter-basket
 */

?>

	

	<footer id="colophon" class="site-footer">
		<div class="row">	
			<p class="col-sm-5"> Какой - то мелкий текст про нас. Все права защищены Большой корпорацией. Сделано с пасхальной радостью. И уважением к покупателю. Сапорты © 2020.
			</p>
			<div class="col-sm-6">  
				<a href="https://m.facebook.com/">
					<img class="social-icons" src="<?php echo get_template_directory_uri() ?> /img/soc-1.png">
				</a>
				<a href="https://adssettings.google.com/">
					<img class="social-icons" src="<?php echo get_template_directory_uri() ?> /img/soc-2.png">
				</a>
				<a href="https://www.instagram.com/">
					<img class="social-icons" src="<?php echo get_template_directory_uri() ?> /img/soc-3.png">
				</a>

			</div>
		</div>		
		</footer><!-- #colophon -->
</body>
</html>
