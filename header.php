
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">
	<link href="https://fonts.googleapis.com/css?family=Philosopher:400,700&display=swap&subset=cyrillic" rel="stylesheet">
	
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

	<nav class="head-line ">
        <div for="checkbox-menu">
        	<div class="easter_logo">
        		<a href="/">
        			<img src="<?php echo get_template_directory_uri(); ?>/img/logo3.png" alt="EA-Basket" title="EA-Basket">
        		</a> 
        		<span class="toggle">☰</span>
        	</div>
        	<nav class="menu_h  mr-4 touch d-flex justify-content-end">
	            <ul>
	                <li><a class="your_basket" href="/order">Твій кошик</a></li>
	                <li><a class="about_us" href="#">Про нас</a></li>
	                <li><a class="opinion" href="/reviews">Відгуки</a></li>
	            </ul>
            </nav>
        </div>
    </nav>

<div class="site-branding">

<body >
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'easter-basket' ); ?></a>

	<header id="masthead" class="site-header">
		
	</header><!-- #masthead -->

	<div id="content" class="site-content">
