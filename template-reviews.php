<?php 
	/* 
	   Template Name: Reviews
	*/ 
	get_header();

	get_template_part('template-parts/content', 'reviews'); 

	get_footer();
?>